import 'package:scoped_model/scoped_model.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:rxdart/subjects.dart';
import 'dart:io';
import 'package:mime/mime.dart';
import 'package:http_parser/http_parser.dart';

import '../models/product.dart';
import '../models/user.dart';
import '../models/auth.dart';
import '../models/location_data.dart';

mixin ConnectedProductsModel on Model {
  List<Product> _products = [];
  User _authenticatedUser;
  String _selectedId;
  bool _isLoading = false;

  void setLoading(bool isLoading) {
    _isLoading = isLoading;
    notifyListeners();
  }
}

mixin ProductsModel on ConnectedProductsModel {
  bool _showFavorites = false;

  List<Product> get allProducts {
    return List.from(_products); 
  }

  List<Product> get productsToDisplay {
    if (_showFavorites)
      return _products.where((p) => p.isFavorite).toList();

    return allProducts;
  }

  bool get isShowingFavorites {
    return _showFavorites;
  }

  String get selectedProductId {
    return _selectedId;
  }

  int get productCount {
    if (_products == null)
      return 0;

    return _products.length;
  }

  int get favoriteProductCount {
    if (_products == null)
      return 0;

    return _products.where((p) => p.isFavorite).length;
  }
 
  Product get selectedProduct {
    if (selectedProductId == null)
      return null;

    return _products.firstWhere((p) => p.id == selectedProductId);
  }

  int get selectedProductPosition {
    return _products.indexWhere((p) => p.id == selectedProductId);
  }

  Future<Map<String, dynamic>> uploadImageFile(File imageFile, {String imagePath}) async {
    final mimeTypeData = lookupMimeType(imageFile.path).split('/');
    final imageUploadRequest = http.MultipartRequest(
      'POST', 
      Uri.parse('https://us-central1-flutter-app-demo-99dfe.cloudfunctions.net/storeImage'));
    final file = await http.MultipartFile.fromPath(
      'image', 
      imageFile.path, 
      contentType: MediaType(
        mimeTypeData[0],
        mimeTypeData[1]));
      
    imageUploadRequest.files.add(file);
    if (imagePath != null)
      imageUploadRequest.fields['imagePath'] = Uri.encodeComponent(imagePath);
    imageUploadRequest.headers['Authorization'] = 'Bearer ${_authenticatedUser.token}';

    try {
      final streamedResponse = await imageUploadRequest.send();
      final response = await http.Response.fromStream(streamedResponse);
      if (response.statusCode != 200 && response.statusCode != 201) {
        print('Something went wrong...');
        print(json.decode(response.body));
        return null;
      }

      final responseData = json.decode(response.body);
      return responseData;
    } catch (error) {
      print(error);
      return null;
    }
  }

  Future<bool> addProducts(String title, String description, double price, File image, LocationData location) async {
    setLoading(true);

    if (location == null)
    return false;

    final imageUploadData = await uploadImageFile(image);
    if (imageUploadData == null) {
      print('Uploadn failed.');
      return false;
    }

    final Map<String, dynamic> productData = {
      'title': title,
      'description': description,
      'price': price,
      'image': imageUploadData['imageUrl'],
      'latitude': location.latitude,
      'longitude': location.longitude,
      'address': location.address,
      'userEmail': _authenticatedUser.email,
      'userId': _authenticatedUser.id,
    };

    try {
      final http.Response response = await http.post(
        'https://flutter-app-demo-99dfe.firebaseio.com/products.json?auth=${_authenticatedUser.token}', 
        body: json.encode(productData));

      bool success = false;
      if (response.statusCode == 200 || response.statusCode == 201) {
        final Map<String, dynamic> responseData = json.decode(response.body);
        if (responseData != null) {
          final Product newProduct = Product(
            id: responseData['name'],
            title: title, 
            description: description, 
            price: price, 
            image: imageUploadData['imageUrl'],
            userEmail: _authenticatedUser.email,
            userId: _authenticatedUser.id,
            location: location,
          );
          
          _products.add(newProduct);
          success = true;
        }
      }

      setLoading(false);
      return success;
    } catch (error) {
      setLoading(false);
      return false;
    }  
  }

  Future<bool> updateProduct(String title, String description, double price, File image, LocationData location, [bool isFavorite]) async {
    setLoading(true);

    String imageUrl = selectedProduct.image;
    if (image != null) {
      final imageUploadData = await uploadImageFile(image);
      if (imageUploadData == null) {
        print('Uploadn failed.');
        return false;
      }

      imageUrl = imageUploadData['imageUrl'];
    }

    final Map<String, dynamic> updatedProductData = {
      'title': title,
      'description': description,
      'price': price,
      'image': imageUrl,
      'userEmail': selectedProduct.userEmail,
      'userId': selectedProduct.userId,
      'address': location.address,
      'latitude': location.latitude,
      'longitude': location.longitude,
    };

    http.Response response = await http.put(
      'https://flutter-app-demo-99dfe.firebaseio.com/products/${selectedProduct.id}.json?auth=${_authenticatedUser.token}', 
      body: json.encode(updatedProductData));
      
    try {
      var success = false;
      if (response.statusCode == 200 || response.statusCode == 201) {
        var updatedProduct = Product(
          id: selectedProduct.id,
          title: title,
          description: description,
          price: price,
          image: imageUrl,
          userEmail: selectedProduct.userEmail,
          userId: selectedProduct.userId,
          location: location,
          isFavorite: isFavorite == null ? selectedProduct.isFavorite : isFavorite,
        );

        _products[selectedProductPosition] = updatedProduct;
        success = true;
      }
      
      setLoading(false);
      return success;
    } catch (error) {
      setLoading(false);
      return false;
    }
  }

  Future<bool> deleteProduct() {
    String selectedProductId = selectedProduct.id;
    _products.removeAt(selectedProductPosition);
    selectProduct(null);
    setLoading(true);
    return http.delete('https://flutter-app-demo-99dfe.firebaseio.com/products/$selectedProductId.json?auth=${_authenticatedUser.token}')
      .then((http.Response response) {
        var success = false;
        if (response.statusCode == 200 || response.statusCode == 201)
          success = true;

        setLoading(false);
        return success;
      }).catchError((error) {
        setLoading(false);
        return false;
      });
  }

  Future<Null> fetchProducts({bool onlyForUser = false, bool clearExisting = false}) {
    if (clearExisting)
      _products = [];
    setLoading(true);
    return http.get('https://flutter-app-demo-99dfe.firebaseio.com/products.json?auth=${_authenticatedUser.token}')
      .then<Null>((http.Response response) {
        if (response.statusCode == 200 || response.statusCode == 201) {
          final List<Product> products = [];
          final Map<String, dynamic> productsMap = json.decode(response.body);
          if (productsMap != null) {
            productsMap.forEach((String key, dynamic value) {
              if (value['userId'] != _authenticatedUser.id && onlyForUser)
                return;

              var userWishlist = (value['wishlistUsers'] as Map<String, dynamic>);
              final Product product = Product(
                id: key,
                title: value['title'],
                description: value['description'],
                price: value['price'] as double,
                image: value['image'],
                userEmail: value['userEmail'],
                userId: value['userId'],
                location: LocationData(
                  address: value['address'],
                  latitude: value['latitude'],
                  longitude: value['longitude'],
                ),
                isFavorite: userWishlist != null 
                  ? userWishlist.containsKey(_authenticatedUser.id)
                  : false,
              );
              
              products.add(product);
            });

            _products = products;
          }
        }

        setLoading(false);
      }).catchError((error) {
        setLoading(false);
        return;
      });
  }

  void selectProduct(String id) {
    _selectedId = id;
    if (id != null)
      notifyListeners();
  }

  Future<bool> toggleProductFavorite() async {
    if (selectedProductId == null)
      return false;

    var isCurrentlyFavorite = selectedProduct.isFavorite;
    var newFavoriteStatus = !isCurrentlyFavorite;
    http.Response response;
    if (newFavoriteStatus) {
      response = await http.put(
        'https://flutter-app-demo-99dfe.firebaseio.com/products/${selectedProduct.id}/wishlistUsers/${_authenticatedUser.id}.json?auth=${_authenticatedUser.token}',
        body: json.encode(true),
      );
    } else {
      response = await http.delete(
        'https://flutter-app-demo-99dfe.firebaseio.com/products/${selectedProduct.id}/wishlistUsers/${_authenticatedUser.id}.json?auth=${_authenticatedUser.token}',
      );
    }

    if (response.statusCode != 200 && response.statusCode != 201) {
      return false;
    }

    final Product product = Product(
      id: selectedProduct.id,
      title: selectedProduct.title,
      description: selectedProduct.description,
      price: selectedProduct.price,
      image: selectedProduct.image,
      userEmail: selectedProduct.userEmail,
      userId: selectedProduct.userId,
      location: selectedProduct.location,
      isFavorite: newFavoriteStatus,
    );
    
    _products[selectedProductPosition] = product;
    //selectProduct(null);
    notifyListeners();

    return true;
  }

  void toggleDisplayFavorite() {
    _showFavorites = !_showFavorites;
    notifyListeners();
  }
}

mixin UserModel on ConnectedProductsModel {
  Timer _authTimer;
  PublishSubject<bool> _userSubject = PublishSubject();

  User get user {
    return _authenticatedUser;
  }

  PublishSubject<bool> get userSubject {
    return _userSubject;
  }

  Future<Map<String, dynamic>> authenticate(String email, String password, [AuthMode mode = AuthMode.Login]) async {
    setLoading(true);

    final Map<String, dynamic> authData = {
      'email': email,
      'password': password,
      'returnSecureToken': true,
    };

    var response;
    if (mode == AuthMode.Login) 
      response = await login(authData);
    else
      response = await signup(authData);

    var isSuccess = false;
    var message = 'There was a problem trying to ${mode == AuthMode.Login ? 'login' : 'signup'}, please try again later.';
    final Map<String, dynamic> responseData = json.decode(response.body);
    if (responseData.containsKey('idToken')) {
      isSuccess = true;
      _authenticatedUser = User(
        id: responseData['localId'],
        email: email,
        password: password,
        token: responseData['idToken'],
      );
      var secondsToExpire = int.parse(responseData['expiresIn']);
      setAuthTimeout(secondsToExpire);
      _userSubject.add(true);
      final SharedPreferences preferences = await SharedPreferences.getInstance();
      final DateTime now = DateTime.now();
      final DateTime expiryTime = now.add(Duration(seconds: secondsToExpire));
      preferences.setString('auth_token', _authenticatedUser.token);
      preferences.setString('user_email', _authenticatedUser.email);
      preferences.setString('user_password', _authenticatedUser.password);
      preferences.setString('user_id', _authenticatedUser.id);
      preferences.setString('token_expiry', expiryTime.toIso8601String());
    } else if (responseData['error']['message'] == 'EMAIL_NOT_FOUND') {
      message = 'This email does not exist!';
    } else if (responseData['error']['message'] == 'INVALID_PASSWORD') {
      message = 'This password is invalid!';
    } else if (responseData['error']['message'] == 'EMAIL_EXISTS') {
      message = 'This email already exists!';
    }

    setLoading(false);

    return {
      'success': isSuccess,
      'message': message,
    };
  }

  Future<http.Response> login(Map<String, dynamic> authData) async {
    return await http.post(
      'https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=AIzaSyCKQQEuZFawffy_k0G_Rpy__dAc7GJ3OlE',
      body: json.encode(authData),
      headers: {
        'Content-Type': 'application/json',
      },
    );
  }

  Future<http.Response> signup(Map<String, dynamic> authData) async {
    return await http.post(
      'https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key=AIzaSyCKQQEuZFawffy_k0G_Rpy__dAc7GJ3OlE',
      body: json.encode(authData),
      headers: {
        'Content-Type': 'application/json',
      },
    );
  }

  void autoAuthenticate() async {
    final SharedPreferences preferences = await SharedPreferences.getInstance();
    var token = preferences.getString('auth_token');
    var expiryTimeString = preferences.getString('token_expiry');
    if (token != null) {
      final DateTime now = DateTime.now();
      var expiryTime = DateTime.parse(expiryTimeString);
      if (expiryTime.isBefore(now)) {
        _authenticatedUser = null;
        notifyListeners();
        return;
      }
      var email = preferences.getString('user_email');
      var password = preferences.getString('user_password');
      var id = preferences.getString('user_id');
      var tokenLifeSpan = expiryTime.difference(now).inSeconds;
      _authenticatedUser = User(
        id: id,
        email: email,
        password: password,
        token: token,
      );
      _userSubject.add(true);
      setAuthTimeout(tokenLifeSpan);
      notifyListeners();
    }
  }

  void logout() async {
    _authenticatedUser = null;
    _authTimer.cancel();
    _userSubject.add(false);
    _selectedId = null;
    final SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.remove('auth_token');
    preferences.remove('user_email');
    preferences.remove('user_password');
    preferences.remove('user_id');
  }

  void setAuthTimeout(int time) {
    _authTimer = Timer(Duration(seconds: time), logout);
  }
}

mixin UtilityModel on ConnectedProductsModel {
  bool get isLoading {
    return _isLoading;
  }
}