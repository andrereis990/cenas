class LocationData {
  final String address;
  final double latitude;
  final double longitude;

  LocationData({
    this.address, 
    this.latitude, 
    this.longitude,
  });
}