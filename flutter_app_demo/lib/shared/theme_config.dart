import 'package:flutter/material.dart';

final ThemeData _androidTheme = ThemeData(
  brightness: Brightness.light,
  primarySwatch: Colors.lightBlue,
  accentColor: Colors.blue,
  cursorColor: Colors.lightBlue,
  buttonTheme: ButtonThemeData(
    buttonColor: Colors.blue,
    textTheme: ButtonTextTheme.primary,
  ),
  appBarTheme: AppBarTheme(
    iconTheme: IconThemeData(
      color: Colors.white,
    ),
    textTheme: TextTheme(
      title: TextStyle(
        color: Colors.white,
        fontSize: 18.0,
      ),
    ),
  ),
  tabBarTheme: TabBarTheme(
    labelColor: Colors.white,
  ),
);

final ThemeData _iosTheme = ThemeData(
  brightness: Brightness.light,
  primarySwatch: Colors.lightBlue,
  accentColor: Colors.blue,
  cursorColor: Colors.lightBlue,
  buttonTheme: ButtonThemeData(
    buttonColor: Colors.blue,
    textTheme: ButtonTextTheme.primary,
  ),
  appBarTheme: AppBarTheme(
    iconTheme: IconThemeData(
      color: Colors.white,
    ),
    textTheme: TextTheme(
      title: TextStyle(
        color: Colors.white,
        fontSize: 18.0,
      ),
    ),
  ),
  tabBarTheme: TabBarTheme(
    labelColor: Colors.white,
  ),
);

ThemeData getAdaptiveTheme(BuildContext context) {
  return Theme.of(context).platform == TargetPlatform.android
    ? _androidTheme
    : _iosTheme;
}
