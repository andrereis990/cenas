import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

import '../widgets/products/products.dart';
import '../scoped_models/main.dart';
import '../widgets/ui_elements/logout_list_tile.dart';
import '../widgets/ui_elements/loading_indicator.dart';
import '../widgets/ui_elements/navigation_header.dart';
import '../shared/global_config.dart';

class ProductsPage extends StatefulWidget {
  final MainModel _model;

  ProductsPage(this._model);

  @override
  State<StatefulWidget> createState() {
    return _ProductsPageState();
  }
}

class _ProductsPageState extends State<ProductsPage> {
  @override
  void initState() {
    widget._model.fetchProducts();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: _buildDrawer(context),
      appBar: AppBar(
        title: Text(gAppName),
        actions: <Widget>[
          ScopedModelDescendant<MainModel>(
            builder: (BuildContext context, Widget child, MainModel model) {
              return IconButton(
                icon: Icon(model.isShowingFavorites ? Icons.favorite : Icons.favorite_border),
                onPressed: () {
                  model.toggleDisplayFavorite();
                },
              );
            },
          ),
        ],
      ),
      body: _buildProducts(),
    );
  }

  Widget _buildDrawer(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          NavigationHeader(gNavHeaderImg),
          ListTile(
            leading: Icon(Icons.edit),
            title: Text('Manage Product'),
            onTap: () {
              Navigator.pushReplacementNamed(context, '/admin');
            },
          ),
          Divider(height: 1.0),
          LogoutListTile(),
        ],
      ),
    );
  }

  Widget _buildProducts() {
    return ScopedModelDescendant<MainModel>(
      builder: (BuildContext context, Widget child, MainModel model) {
        return RefreshIndicator(
          onRefresh: model.fetchProducts,
          child: model.isLoading 
            ? LoadingIndicator()
            : Products(),
        );
      },
    );
  }
}