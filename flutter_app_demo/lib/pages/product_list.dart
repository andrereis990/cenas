import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

import './product_edit.dart';
import '../models/product.dart';
import '../scoped_models/main.dart';
import '../widgets/ui_elements/loading_indicator.dart';

class ProductListPage extends StatefulWidget {
  final MainModel model;

  ProductListPage(this.model);

  @override
  State<StatefulWidget> createState() {
    return _ProductListPageState();
  }
}

class _ProductListPageState extends State<ProductListPage> {

  @override
  void initState() {
    widget.model.fetchProducts(
      onlyForUser: true, 
      clearExisting: true);
      
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<MainModel>(
      builder: (BuildContext context, Widget child, MainModel model) {
        return model.isLoading 
          ? LoadingIndicator()
          : ListView.builder(
              itemCount: model.productCount,
              itemBuilder: (BuildContext context, int index) {
                Product product = model.allProducts[index];
                return Dismissible(
                  key: Key(product.id),
                  onDismissed: (DismissDirection direction) {
                    if (direction == DismissDirection.endToStart || direction == DismissDirection.startToEnd)
                    {
                      model.selectProduct(product.id);
                      model.deleteProduct();
                    }
                  },
                  background: Container(
                    color: Colors.red,
                  ),
                  child: Column(
                    children: <Widget>[
                      ListTile(
                        leading: CircleAvatar(
                          backgroundImage: NetworkImage(product.image),
                        ),
                        title: Text(product.title),
                        subtitle: Text('\$${product.price.toStringAsFixed(2)}'),
                        trailing: _buildEditIcon(context, product.id, model),
                      ),
                      Divider(height: 2),
                    ],
                  ),
                );
              },
            );
      },
    );
  }

  Widget _buildEditIcon(BuildContext context, String id, MainModel model) {
    return IconButton(
      icon: Icon(Icons.edit),
      onPressed: () {
        model.selectProduct(id);
        Navigator.of(context).push(MaterialPageRoute(
          builder: (BuildContext context) {
            return ProductEditPage();
          }
        )).then((_) => model.selectProduct(null));
      },
    );
  }
}