import 'package:flutter/animation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

import '../scoped_models/main.dart';
import '../models/auth.dart';
import '../widgets/ui_elements/loading_indicator.dart';

class AuthPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _AuthPageState();
  }
}

class _AuthPageState extends State<AuthPage> with TickerProviderStateMixin {
  final TextEditingController _passwordTextFieldController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final Map<String, dynamic> _formData = {
    'email': null,
    'password': null,
    'acceptedTerms': false,
  };

  AuthMode _authMode = AuthMode.Login;
  AnimationController _controller;    
  Animation<Offset> _slideAnimation;

  @override
  void initState() {
    _controller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 300)
    );
    _slideAnimation = Tween<Offset>(
      begin: Offset(0.0, -2.0),
      end: Offset.zero,
    ).animate(CurvedAnimation(
      parent: _controller,
      curve: Curves.fastOutSlowIn,
    ));
    super.initState();
  }

  void _authenticateUser(Function authenticate) async {
    if (!_formKey.currentState.validate() || !_formData['acceptedTerms'])
      return;

    _formKey.currentState.save();

    Map<String, dynamic> response = await authenticate(_formData['email'], _formData['password'], _authMode);   

    if (!response['success'])
      _showErrorDialog(response['message']);
  }

  void _switchAuthMode() {
    setState(() {
      _authMode = _authMode == AuthMode.Login
        ? AuthMode.Signup
        : AuthMode.Login;
    });
    _authMode == AuthMode.Signup
      ? _controller.forward()
      : _controller.reverse();
  }

  void _showErrorDialog(String message) {
    showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('An error occurred!'),
              content: Text(message),
              actions: <Widget>[
                FlatButton(
                  child: Text('Ok'),
                  onPressed: () {
                    Navigator.of(context).pop(context);
                  },
                ),
              ],
            );
          },
        );
  }

  @override
  Widget build(BuildContext context) {
    var isLandscapeMode = MediaQuery.of(context).orientation == Orientation.landscape;

    return Scaffold(
      appBar: AppBar(
        title: Text('Login'),
      ),
      body: Container(
        decoration: _buildBackgroundImage(),
        child: Form(
          key: _formKey,
          child: isLandscapeMode ? _buildLandscapeLayout() : _buildPortraitLayout(),
        ),
      ),
    );
  }

  Widget _buildLandscapeLayout() {
    return Row(
      children: <Widget>[
        Flexible(
          flex: 1,
          child: ListView(
            padding: EdgeInsets.only(left: 18.0, top: 6.0),
            children: <Widget>[
              _buildEmailTextField(),
              _buildPasswordTextField(),
              _buildPasswordConfirmationTextField(),
              _buildAcceptTermsSwitch(true),
              _buildModeSwitchWidget(),
            ],
          ),
        ),
        Flexible(
          flex: 1,
          child: Center(
            child: Column(
              children: <Widget>[
                _buildLogoImageContainer(),
                _buildLoginButton(true),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildPortraitLayout() {
    return ListView(
      children: <Widget>[
        _buildLogoImageContainer(),
        _buildEmailTextField(),
        _buildPasswordTextField(),
        _buildPasswordConfirmationTextField(),
        _buildAcceptTermsSwitch(false),
        _buildModeSwitchWidget(),
        _buildLoginButton(false),
      ],
    );
  }

  BoxDecoration _buildBackgroundImage() {
    return BoxDecoration(
      color: Colors.white,
      // image: DecorationImage(
      //   image: AssetImage('assets/background.jpg'),
      //   repeat: ImageRepeat.repeat,
      //   colorFilter: ColorFilter.mode(
      //     Colors.black.withOpacity(0.3), 
      //     BlendMode.dstATop,
      //   ),
      // ),
    );
  }

  Widget _buildLogoImageContainer() {
    return Container(
      margin: EdgeInsets.all(18.0),
      child: Image.asset('assets/lion.jpg',
        width: 150.0,
        height: 150.0,
      ),
    );
  }

  Widget _buildEmailTextField() {
    return Container(
      margin: EdgeInsets.fromLTRB(22.0, 0.0, 22.0, 0.0),
      child: TextFormField(
        keyboardType: TextInputType.emailAddress,
        decoration: InputDecoration(
          labelText: 'Email',
        ),
        onSaved: (String value) {
          _formData['email'] = value;
        },
        validator: (String value) {
          if (value == null || value.isEmpty)
            return 'The email field is required.';
          else if (!RegExp(r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?").hasMatch(value))
            return 'The email given is invalid.';
        },
      ),
    );
  }

  Widget _buildPasswordConfirmationTextField() {
    return FadeTransition(
      opacity: CurvedAnimation(
        parent: _controller,
        curve: Curves.easeIn,
      ),
      child: SlideTransition(
        position: _slideAnimation,
        child: Container(
          margin: EdgeInsets.fromLTRB(22.0, 0.0, 22.0, 0.0),
          child: TextFormField(
            keyboardType: TextInputType.text,
            obscureText: true,
            decoration: InputDecoration(
              labelText: 'Confirm Password',
            ),
            validator: (String value) {
              if (_authMode == AuthMode.Signup){
                if (value == null || value.isEmpty)
                  return 'The confirm password field is required.';
                else if (value != _passwordTextFieldController.text)
                  return 'The passwords do not match.';
              }
            },
          ),
        ),
      ),
    );
  }

  Widget _buildPasswordTextField() {
    return Container(
      margin: EdgeInsets.fromLTRB(22.0, 0.0, 22.0, 0.0),
      child: TextFormField(
        obscureText: true,
        controller: _passwordTextFieldController,
        decoration: InputDecoration(
          labelText: 'Password',
        ),
        onSaved: (String value) {
          _formData['password'] = value; 
        },
        validator: (String value) {
          if (value == null || value.isEmpty)
            return 'The password field is required.';
          else if (value.length < 6)
            return 'The password must be at least 6 characters long.';
        },
      ),
    );
  }

  Widget _buildAcceptTermsSwitch(bool isLandscapeMode) {
    return Container(
      margin: EdgeInsets.only(left: isLandscapeMode ? 6.0 : 10.0),
      child: SwitchListTile(
        title: Text('Accept Terms'),
        value: _formData['acceptedTerms'],
        onChanged: (bool value) {
          setState(() {
            _formData['acceptedTerms'] = value;
          });
        },
      ),
    );
  }

  Widget _buildModeSwitchWidget() {
    return Container(
      alignment: Alignment.topLeft,
      child: FlatButton(
        child: Text('Switch to ${_authMode == AuthMode.Login ? 'Signup' : 'Login'}'),
        onPressed: _switchAuthMode,
      ),
    );
  }

  Widget _buildLoginButton(bool isLandscapeMode) {
    return Container(
      margin: EdgeInsets.fromLTRB(22.0, isLandscapeMode ? 11.0 : 6.0, 22.0, 0.0),
      child: ScopedModelDescendant<MainModel>(
        builder: (BuildContext context, Widget child, MainModel model) {
          return model.isLoading
            ? LoadingIndicator()
            : RaisedButton(
              padding: EdgeInsets.only(left: 66.0, right: 66.0),
              child: Text(_authMode == AuthMode.Login ? 'Login' : 'Signup'),
              onPressed: () => _authenticateUser(model.authenticate),
            );
        },
      ),
    );
  }
}
