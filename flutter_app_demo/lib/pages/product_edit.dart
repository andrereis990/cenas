import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'dart:io';

import '../models/product.dart';
import '../scoped_models/main.dart';
import '../widgets/form_inputs/location.dart';
import '../models/location_data.dart';
import '../widgets/form_inputs/image.dart';
import '../widgets/ui_elements/loading_indicator.dart';

class ProductEditPage extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return _ProductEditPageState();
  }
}

class _ProductEditPageState extends State<ProductEditPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  
  final _titleTextController = TextEditingController();
  final _descriptionTextController = TextEditingController();
  final _priceTextController = TextEditingController();

  LocationData _location;
  File _image;

  void _setLocation(LocationData location) {
    _location = location;
  }

  void _setImage(File imageSource) {
    _image = imageSource;
  }

  void _submitForm(Function addProduct, Function updateProduct, Function setSelectedProduct, [String selectedProductId]) { 
    if (!_formKey.currentState.validate() || (_image == null && selectedProductId == null))
      return;
    
    _formKey.currentState.save();

    if (selectedProductId == null)
      addProduct(
        _titleTextController.text, 
        _descriptionTextController.text, 
        double.parse(_priceTextController.text.replaceFirst(RegExp(r','), '.')), 
        _image,
        _location,
      ).then((bool success) {
        if (success) 
          Navigator.pushReplacementNamed(context, '/products').then((_) => setSelectedProduct(null));
        else
          showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text('Something went wrong!'),
                content: Text('We were unable to create a new product, try again later!'),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Ok'),
                    onPressed: () => Navigator.of(context).pop(),
                  ),
                ],
              );
            },
          );
        }
      );
    else
      updateProduct(
        _titleTextController.text, 
        _descriptionTextController.text, 
        double.parse(_priceTextController.text.replaceFirst(RegExp(r','), '.')), 
        _image,
        _location,
      ).then((_) => Navigator.pushReplacementNamed(context, '/products')
        .then((_) => setSelectedProduct(null)));
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<MainModel>(
      builder: (BuildContext context, Widget child, MainModel model) {
        final Widget pageContent = _buildPageContent(
          model.selectedProduct, 
          model.addProducts, 
          model.updateProduct,
        );
        
        return model.selectedProductId == null ?
          pageContent : 
          Scaffold(
            appBar: AppBar(
              title: Text('Edit Product'),
            ),
            body: pageContent,
          );
      },
    );
  }

  Widget  _buildPageContent(Product product, Function addProducts, Function deleteProduct) {
    return Container(
      margin: EdgeInsets.all(10.0),
      child: Form(
        key: _formKey,
        child: ListView(
          children: <Widget>[
            _buildTitleTextField(product == null ? _titleTextController.text : product.title),
            _buildDescriptionTextField(product == null ? _descriptionTextController.text : product.description),
            _buildPriceTextField(product == null ? _priceTextController.text : product.price.toStringAsFixed(2)),
            SizedBox(height: 10.0),
            LocationInput(_setLocation, product),
            SizedBox(height: 10.0),
            ImageInput(_setImage, product),
            SizedBox(height: 10.0),
            _buildSubmitButtom(),
          ],
        ),
      ),
    );
  }

  Widget _buildTitleTextField(String title) {

    _titleTextController.text = title;

    return TextFormField(
      controller: _titleTextController,
      decoration: InputDecoration(
        labelText: 'Product Title'
      ),
      validator: (String value) {
        if (value == null || value.isEmpty)
          return 'The title field is required.';
        else if (value.length > 23)
          return 'The max title length is 23 characters.';
      },
    );
  }

  Widget _buildDescriptionTextField(String description) {
    _descriptionTextController.text = description;
    return TextFormField(
      controller: _descriptionTextController,
      maxLines: 4,
      decoration: InputDecoration(
        labelText: 'Product Description'
      ),
      validator: (String value) {
        if (value == null || value.isEmpty)
          return 'The description field is required.';
      },
    );
  }

  Widget _buildPriceTextField(String price) {
    _priceTextController.text = price;
    return TextFormField(
        controller: _priceTextController,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          labelText: 'Product Price'
        ),
        validator: (String value) {
          if (value == null || value.isEmpty)
            return 'The price field is required.';
          else if (double.tryParse(value.replaceFirst(RegExp(r','), '.')) == null)
            return 'The price value is invalid.';
        },
      );
  }

  Widget _buildSubmitButtom() {
    return ScopedModelDescendant<MainModel>(
      builder: (BuildContext context, Widget child, MainModel model) {
        return model.isLoading 
          ? LoadingIndicator() 
          : RaisedButton(
              child: Text('Save'),
              onPressed: () => _submitForm(model.addProducts, model.updateProduct, model.selectProduct, model.selectedProductId)
            );
      },
    );
  }
}