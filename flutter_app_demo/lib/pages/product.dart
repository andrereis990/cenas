import 'package:flutter/material.dart';
import 'dart:async';
import 'package:scoped_model/scoped_model.dart';
import 'package:map_view/map_view.dart';

import '../widgets/ui_elements/title_default.dart';
import '../scoped_models/main.dart';
import '../models/product.dart';
import '../widgets/products/product_fab.dart';

class ProductPage extends StatelessWidget {
  final Product product;

  ProductPage(this.product);

  void _showMap() {
    final markers = <Marker>[
      Marker(
        'position',
        'Position',
        product.location.latitude,
        product.location.longitude,
      ),
    ];
    final cameraPosition = CameraPosition(
      Location(
        product.location.latitude,
        product.location.longitude,
      ),
      14.0
    );
    final mapView = MapView();
    mapView.show(
      MapOptions(
        initialCameraPosition: cameraPosition,
        mapViewType: MapViewType.normal,
        title: 'Product Location',
      ),
      toolbarActions: [
        ToolbarAction('Close', 1),
      ],
    );
    mapView.onToolbarAction.listen((int id) {
      if (id == 1) {
        mapView.dismiss();
      }
    });
    mapView.onMapReady.listen((_) {
      mapView.setMarkers(markers);
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        Navigator.pop(context, false);
        return Future.value(false);
      },
      child: ScopedModelDescendant<MainModel>(
        builder: (BuildContext context, Widget child, MainModel model) {
          bool isPortraitMode = MediaQuery.of(context).orientation == Orientation.portrait;
          double imageHeight = MediaQuery.of(context).size.height * (isPortraitMode ? 0.36 : 0.5);

          return Scaffold(
            body: CustomScrollView(
              slivers: <Widget>[
                SliverAppBar(
                  expandedHeight: imageHeight,
                  title: Text(product.title,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 22.0,
                    ),
                  ),
                  pinned: true,
                  flexibleSpace: FlexibleSpaceBar(
                    centerTitle: false,
                    background: Hero(
                      tag: product.id,
                      child: FadeInImage(
                        image: AssetImage('assets/image_test.jpg'), //NetworkImage(product.image),
                        height: imageHeight,
                        fit: BoxFit.cover,
                        placeholder: AssetImage('assets/image_test.jpg'),
                      ),
                    ),
                  ),
                ),
                SliverList(
                  delegate: SliverChildListDelegate([
                    Container(
                      padding: EdgeInsets.all(10.0),
                      child: Center(
                        child: TitleDefault(product.title),
                      ),
                    ),
                    _buildLocationAndPriceRow(product.location.address, '\$ ${product.price.toStringAsFixed(2)}'),
                    _buildDescriptionContainer(product.description),
                  ]),
                ),
              ],
            ),
            floatingActionButton: ProductFab(),
          );
        },
      ),
    );
  }

  Widget _buildLocationAndPriceRow(String location, String price) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        GestureDetector(
          onTap: _showMap,
          child: Text(location,
            style: TextStyle(
              fontFamily: 'Oswald',
              color: Colors.grey,
            ),
          ),
        ),
        Text(' | '),
        Text(price,
          style: TextStyle(
            fontFamily: 'Oswald',
            color: Colors.grey,
          ),
        ),
      ],
    );
  }

  Widget _buildDescriptionContainer(String description) {
    return Container(
      margin: EdgeInsets.only(top: 12.0),
      padding: EdgeInsets.all(6.0),
      child: Text(description, 
        textAlign: TextAlign.justify,
      ),
    );
  }
}
