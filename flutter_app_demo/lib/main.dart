import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:map_view/map_view.dart';
//import 'package:flutter/rendering.dart';

import './pages/auth.dart';
import './pages/products_admin.dart';
import './pages/products.dart';
import './pages/product.dart';
import './scoped_models/main.dart';
import './models/product.dart';
import './widgets/helpers/custom_route.dart';
import './shared/global_config.dart';
import './shared/theme_config.dart';

main() {
  //debugPaintSizeEnabled = true;
  //debugPaintBaselinesEnabled = true;
  //debugPaintPointersEnabled = true;
  MapView.setApiKey(gApiKey);
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {
  final MainModel _model = MainModel();
  //final _platformChannel = MethodChannel('flutter-app-demo.com/battery');

  bool _isAuthenticated = false;

  // Future<Null> _getBatteryLevel() async {
  //   String batteryLevel;
  //   try {
  //     final int result = await _platformChannel.invokeMethod('getBatteryLevel');
  //     batteryLevel = 'Battery level is ${result.toString()}';
  //   } catch (error) {
  //     batteryLevel = 'Unable to get battery level.}';
  //   }

  //   print(batteryLevel);
  // }

  @override
  void initState() {
    _model.autoAuthenticate();
    _model.userSubject.listen((bool isAuthenticated) {
      setState(() {
       _isAuthenticated = isAuthenticated; 
      });
    });
    //_getBatteryLevel();
    super.initState();
  }
  
  @override
  Widget build(BuildContext context) {
    return ScopedModel<MainModel>(
      model: _model,
      child: MaterialApp(
        //debugShowMaterialGrid: true,
        title: 'Flutter App Demo',
        theme: getAdaptiveTheme(context),
        routes: {
          '/': (BuildContext context) => _isAuthenticated ? ProductsPage(_model) : AuthPage(),
          '/admin': (BuildContext context) => _isAuthenticated ? ProductsAdminPage(_model) : AuthPage(),
        },
        onGenerateRoute: (RouteSettings settings) {
          if (!_isAuthenticated) {
            return MaterialPageRoute<bool>(
              builder: (BuildContext context) => AuthPage(),
            );
          }

          final List<String> pathElements = settings.name.split('/');
          if (pathElements[0] != '')
            return null;
          
          if (pathElements[1] == 'product'){
            final String id = pathElements[2];
            final Product product = _model.allProducts.firstWhere((p) => p.id == id);
            return CustomRoute<bool>(
              builder: (BuildContext context) => _isAuthenticated ? ProductPage(product) : AuthPage(),
            );
          }  

          return null;      
        },
        onUnknownRoute: (RouteSettings settings) {
          return MaterialPageRoute(
            builder: (BuildContext context) => _isAuthenticated ? ProductsPage(_model) : AuthPage()
          );
        },
      ),
    );
  }
}
