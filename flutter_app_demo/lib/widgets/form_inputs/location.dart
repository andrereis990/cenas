import 'package:flutter/material.dart';
import 'package:map_view/map_view.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:location/location.dart' as geo;
import 'dart:async';

import '../helpers/ensure_visible.dart';
import '../../models/location_data.dart';
import '../../models/product.dart';
import '../../shared/global_config.dart';

class LocationInput extends StatefulWidget {
  final Function setLocation;
  final Product product;

  LocationInput(this.setLocation, this.product);

  @override
  State<StatefulWidget> createState() {
    return _LocationInputState();
  }
}

class _LocationInputState extends State<LocationInput> {
  Uri _staticMapUri;
  LocationData _locationData;
  final FocusNode _addressInputFocusNode = FocusNode();
  final TextEditingController _addressInputController = TextEditingController();

  @override
  void initState() {
    _addressInputFocusNode.addListener(_updateLocation);
    if (widget.product != null)
      _getStaticMap(widget.product.location.address, isGeocode: false);
    super.initState();
  }

  @override
  void dispose() {
    _addressInputFocusNode.removeListener(_updateLocation);
    super.dispose();
  }

  void _getStaticMap(String address, {double latitude, double longitude, bool isGeocode = true}) async {
    if (address.trim().isEmpty || !mounted) {
      setState(() {
        _staticMapUri = null;
      });
      _locationData = LocationData(
        address: 'Santuário Nacional de Cristo Rei',
        latitude: 38.404289,
        longitude: 9.101680,
      );
      widget.setLocation(_locationData);
      return;
    }

    if (isGeocode) {
      final Uri uri = Uri.https(
        'maps.googleapis.com', 
        '/maps/api/geocode/json',
        {
          'address': address,
          'key': gApiKey,
        }
      );

      final http.Response response = await http.get(uri);
      final decodedResponse = json.decode(response.body);

      if ((decodedResponse['results'] as List<dynamic>).length > 0) {
        final formattedAddress = decodedResponse['results'][0]['geometry']['formatted_address'];
        final coordinates = decodedResponse['results'][0]['geometry']['location'];
        _locationData = LocationData(
          address: formattedAddress,
          latitude: coordinates['lat'],
          longitude: coordinates['lng'],
        );
      } else {
        _locationData = LocationData(
          address: 'Santuário Nacional de Cristo Rei',
          latitude: 38.404289,
          longitude: 9.101680,
        );
      }
    } else if (latitude == null || longitude == null) {
      _locationData = widget.product.location;
    } else {
      _locationData = LocationData(
        address: address, 
        latitude: latitude,
        longitude: longitude,
      );
    }

    final StaticMapProvider staticMapProvider = StaticMapProvider(gApiKey);
    final Uri staticMapUri = staticMapProvider.getStaticUriWithMarkers(
      [
        Marker('position', 'Position', _locationData.latitude, _locationData.longitude),
      ],
      center: Location(_locationData.latitude, _locationData.longitude),
      width: 500,
      height: 300,
      maptype: StaticMapViewType.roadmap
    );

    widget.setLocation(_locationData);
    setState(() {
      _addressInputController.text = _locationData.address;
      _staticMapUri = staticMapUri;
    });
  }

  Future<String> _getAddress(double latitude, double longitude) async {
    final uri = Uri.https(
      'maps.googleapis.com', 
      '/maps/api/geocode/json',
      {
        'latlng': '${latitude.toString()},${longitude.toString()}',
        'key': gApiKey,
      }
    );

    final http.Response response = await http.get(uri);
    final decodedResponse = json.decode(response.body);
    final formattedAddress = decodedResponse['results'][0]['formatted_address'];
    return formattedAddress;
  }

  void _getUserLocation() async {
    try {
      final location = geo.Location();
      final currentLocation = await location.getLocation();
      final address = await _getAddress(currentLocation.latitude, currentLocation.longitude);
      _getStaticMap(address, isGeocode: false, latitude: currentLocation.latitude, longitude: currentLocation.longitude);
    } catch (error) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Could not fetch location'),
            content: Text('Try to add the address manually.'),
            actions: <Widget>[
              FlatButton(
                child: Text('OK'),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          );
        }
      );
    }
  }

  void _updateLocation () {
    if (!_addressInputFocusNode.hasFocus) {
      _getStaticMap(_addressInputController.text);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        EnsureVisibleWhenFocused(
          focusNode: _addressInputFocusNode,
          child: TextFormField(
            focusNode: _addressInputFocusNode,
            controller: _addressInputController,
            validator: (String value) {
              // if (value.isEmpty) {
              //   return 'No valid location found.';
              // }
            },
            decoration: InputDecoration(
              labelText: 'Address'
            ),
          ),
        ),
        SizedBox(height: 10.0),
        FlatButton(
          child: Text('Locate Me'),
          onPressed: _getUserLocation,
        ),
        SizedBox(height: 10.0),
        _staticMapUri == null ? Container() : Image.network(_staticMapUri.toString()),
      ],
    );
  }
}