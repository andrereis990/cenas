import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';

import '../../models/product.dart';

class ImageInput extends StatefulWidget {
  final Function setImage;
  final Product product;

  ImageInput(this.setImage, this.product);

  @override
  State<StatefulWidget> createState() {
    return _ImageInputState();
  }
}

class _ImageInputState extends State<ImageInput> {
  File _imageFile;

  void _getImage(ImageSource source) async {
    File image = await ImagePicker.pickImage(source: source, maxWidth: 400.0);
    setState(() {
      _imageFile = image;
    });
    widget.setImage(image);
    Navigator.pop(context);	
  }
  
  void _openImagePicker() {
    showModalBottomSheet(
      context: context,
      builder: (BuildContext context) {
        return Container(
          padding: EdgeInsets.all(10.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text('Pick an Image'),
              SizedBox(height: 10.0),
              FlatButton(
                child: Text('Use Camera'),
                onPressed: () {
                  _getImage(ImageSource.camera);
                },
              ),
              FlatButton(
                child: Text('Use Gallery'),
                onPressed: () {
                  _getImage(ImageSource.gallery);
                },
              ),
              SizedBox(height: 10.0),
            ],
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        OutlineButton(
          onPressed: _openImagePicker,
          borderSide: BorderSide(
            color: Theme.of(context).accentColor,
            width: 2.0,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(Icons.camera_alt),
              SizedBox(width: 5.0,),
              Text('Add Image'),
            ],
          ),
        ),
        SizedBox(height: 10.0),
        _buildImageContainer(),
      ],
    );
  }

  Widget _buildImageContainer() {
    return _imageFile != null 
        ? Image.file(_imageFile,
            fit: BoxFit.cover,
            height: 300.0,
            width: MediaQuery.of(context).size.width,
            alignment: Alignment.center)
        : widget.product != null 
          ? Image.network(widget.product.image,
              fit: BoxFit.cover,
              height: 300.0,
              width: MediaQuery.of(context).size.width,
              alignment: Alignment.center)
          : Text('Please pick an image.');
  }
}