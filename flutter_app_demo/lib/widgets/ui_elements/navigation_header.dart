import 'package:flutter/material.dart';

class NavigationHeader extends StatelessWidget {
  final String imageUrl;

  NavigationHeader(this.imageUrl);

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    // bool isPortraitMode = MediaQuery.of(context).orientation == Orientation.portrait;
    // double widthToOccupy = isPortraitMode
    //   ? screenWidth * 1
    //   : screenWidth * 0.3;

    return Container(
      height: 220.0,
      width: screenWidth,
      padding: EdgeInsets.all(12.0),
      color: Colors.lightBlue,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            alignment: Alignment.centerLeft,
            child: CircleAvatar(
              backgroundImage: AssetImage('assets/image_test.jpg'), //NetworkImage(imageUrl),
              radius: 50.0,
            ),
          ),
          SizedBox(height: 22.0),
          Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Text(
                'André dos Reis',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 20.0,
                ),
              ),
              Flexible(
                child: Container(),
                flex: 1,
                fit: FlexFit.tight,
              ),
              Text(
                'v1.0.0.0',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 10.0,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}