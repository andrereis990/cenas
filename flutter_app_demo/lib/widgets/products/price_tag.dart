import 'package:flutter/material.dart';

class PriceTag extends StatelessWidget {
  final String _price;

  PriceTag(this._price);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 4.0),
      padding: EdgeInsets.symmetric(horizontal: 6.0, vertical: 3),
      decoration: BoxDecoration(
        border: Border.all(
          width: 1.6,
          color: Theme.of(context).accentColor,
        ),
        borderRadius: BorderRadius.circular(4.0),
      ),
      child: Text(_price,
        style: TextStyle(
          color: Theme.of(context).accentColor,
        ),
      ),
    );
  }
}