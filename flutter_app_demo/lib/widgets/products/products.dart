import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

import './product_card.dart';
import '../../models/product.dart';
import '../../scoped_models/main.dart';

class Products extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<MainModel>(
      builder: (BuildContext context, Widget child, MainModel model) {
        return _buildList(model.productsToDisplay, context);
      },
    );
  }

  Widget _buildList(List<Product> products, BuildContext context) {
    bool isPortraitMode = MediaQuery.of(context).orientation == Orientation.portrait;

    Widget productCard = Center(child: Text('No products found, please add some!'));
    if (products.length > 0)
      productCard = ListView.builder(
        scrollDirection: isPortraitMode 
          ? Axis.vertical
          : Axis.horizontal,
        itemBuilder: (BuildContext context, int index) => ProductCard(products[index]), 
        itemCount: products.length
      );

    return productCard;
  }
}
