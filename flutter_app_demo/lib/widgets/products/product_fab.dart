import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:url_launcher/url_launcher.dart';
import 'dart:math' as math;

import '../../scoped_models/main.dart';

class ProductFab extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ProductFabState();
  }
}

class _ProductFabState extends State<ProductFab> with TickerProviderStateMixin {
  AnimationController _controller;
  bool _isFavoriting = false;

  @override
  void initState() {
    _controller = AnimationController(
      vsync: this, 
      duration: Duration(milliseconds: 200)
    );
    super.initState();
  }

  void _onContactClick(MainModel model) async {
    final url = 'mailto:${model.selectedProduct.userEmail}';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      
    }
  }

  void _onFavoriteClick(MainModel model) {
    setState(() {
      _isFavoriting = true;
    });
    model.toggleProductFavorite().then((_) {
      setState(() {
        _isFavoriting = false;
      });
    });
  }

  void _onOptionsClick() {
    if (_controller.isDismissed)
      _controller.forward();
    else
      _controller.reverse();
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<MainModel>(
      builder: (BuildContext context, Widget child, MainModel model) {
        bool isPortraitMode = MediaQuery.of(context).orientation == Orientation.portrait;

        return isPortraitMode
          ? Column(
            mainAxisSize: MainAxisSize.min,
            children: _buildFloatingButtons(model),
          ) : Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: _buildFloatingButtons(model),
          );
      },
    );
  }

  List<Widget> _buildFloatingButtons(MainModel model) {
    return [
      Container(
        height: 60.0,
        width: 56.0,
        alignment: FractionalOffset.topCenter,
        child: ScaleTransition(
          scale: CurvedAnimation(
            parent: _controller,
            curve: Interval(0.0, 1.0, 
              curve: Curves.easeOut,
            ),
          ),
          child: FloatingActionButton(
            mini: true,
            heroTag: 'contact',
            child: Icon(
              Icons.mail_outline,              
              color: Theme.of(context).primaryColor
            ),
            backgroundColor: Theme.of(context).cardColor,
            onPressed: () {
              _onContactClick(model);
            },
          )
        ),
      ),
      Container(
        height: 60.0,
        width: 56.0,
        alignment: FractionalOffset.topCenter,
        child: ScaleTransition(
          scale: CurvedAnimation(
            parent: _controller,
            curve: Interval(0.0, 0.5, 
              curve: Curves.easeOut,
            ),
          ),
          child: FloatingActionButton(
            mini: true,
            heroTag: 'favorite',
            child: _isFavoriting
              ? Container(
                margin: EdgeInsets.all(10.0),
                child: CircularProgressIndicator(
                  strokeWidth: 3.0,
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.red),
                ))
              : Icon(
                model.selectedProduct.isFavorite 
                  ? Icons.favorite
                  : Icons.favorite_border, 
                color: Colors.red),
            backgroundColor: Theme.of(context).cardColor,
            onPressed: () {
              if (!_isFavoriting)
                _onFavoriteClick(model);
            },
          ),
        ),
      ),
      Container(
        height: 70.0,
        width: 56.0,
        child: FloatingActionButton(
          mini: true,
          heroTag: 'options',
          child: AnimatedBuilder(
            animation: _controller,
            builder: (BuildContext xontext, Widget child) {
              return Transform(
                alignment: FractionalOffset.center,
                transform: Matrix4.rotationZ(_controller.value * 1 * math.pi),
                child: Icon(_controller.isDismissed
                  ? Icons.more_vert
                  : Icons.close
                ),
              );
            },
          ),
          onPressed: () {
            _onOptionsClick();
          },
        ),
      )
    ];
  }

}