import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

import './price_tag.dart';
import '../ui_elements/title_default.dart';
import '../../models/product.dart';
import '../../scoped_models/main.dart';

class ProductCard extends StatefulWidget {
  final Product product;

  ProductCard(this.product);

  @override
  State<StatefulWidget> createState() {
    return _ProductCardState();
  }
}

class _ProductCardState extends State<ProductCard> {
  bool _isFavoriting = false;

  void onProductDetailClick(BuildContext context, MainModel model) {
    model.selectProduct(widget.product.id);
    Navigator.pushNamed<bool>(
      context,
      '/product/' + widget.product.id,
    ).then((_) {
      model.selectProduct(null);
    });
  }

  void onAddToFavClick(MainModel model) {
    setState(() {
      _isFavoriting = true;
    });
    model.selectProduct(widget.product.id);
    model.toggleProductFavorite().then((_) {
      model.selectProduct(null);
      setState(() {
        _isFavoriting = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    bool isPortraitMode = MediaQuery.of(context).orientation == Orientation.portrait;

    return Container(
      margin: EdgeInsets.all(6.0),
      child: isPortraitMode 
        ? _buildVerticalListCard()
        : _buildHorizontalListCard(),
    );
  }

  Widget _buildHorizontalListCard() {
    double imageWidth = MediaQuery.of(context).size.width * 0.42;
    double imageHeight = MediaQuery.of(context).size.height * 0.38;

    return Card(
      child: Column(
        children: <Widget>[
          Hero(
            tag: widget.product.id,
            child: Container(
              child: FadeInImage(
                image: AssetImage('assets/image_test.jpg'), //NetworkImage(widget.product.image, scale: 2),
                placeholder: AssetImage('assets/image_test.jpg'),
                height: imageHeight,
                width: imageWidth,
                fit: BoxFit.cover,
              ),
            ),
          ),
          _buildTitleAndPriceContainer(imageWidth),
          _buildDetailsContainer(widget.product.location.address, imageWidth),
          _buildButtonRow(context, imageWidth),
        ],
      ),
    );
  }

  Widget _buildVerticalListCard() {
    double imageWidth = MediaQuery.of(context).size.width * 0.99;
    double imageHeight = MediaQuery.of(context).size.height * 0.26;

    return Card(
      child: Column(
        children: <Widget>[
          Hero(
            tag: widget.product.id,
            child: Container(
              child: FadeInImage(
                image: AssetImage('assets/image_test.jpg'), //NetworkImage(widget.product.image, scale: 2),
                placeholder: AssetImage('assets/image_test.jpg'),
                height: imageHeight,
                width: imageWidth,
                fit: BoxFit.cover,
              ),
            ),
          ),
          _buildTitleAndPriceContainer(imageWidth),
          _buildDetailsContainer(widget.product.location.address, imageWidth),
          _buildButtonRow(context, imageWidth),
        ],
      ),
    );
  }

  Widget _buildTitleAndPriceContainer(double width) {
    return Container(
      alignment: Alignment.centerLeft,
      width: width,
      padding: EdgeInsets.only(top: 6.0),
      child: Row(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(left: 12.0, right: 6.0),
            child: TitleDefault(widget.product.title),
          ),
          PriceTag('\$ ${widget.product.price.toStringAsFixed(2)}'),
        ],
      ),
    );
  }

  Widget _buildDetailsContainer(String location, double width) {
    return Flexible(
      flex: 0,
      fit: FlexFit.tight,
      child: Container(
        alignment: Alignment.centerLeft,
        width: width,
        padding: EdgeInsets.only(left: 12.0, top: 12.0),
        child: Column(
          children: <Widget>[
            Text(location)
          ],
        ),
      ),
    );
  }

  Widget _buildButtonRow(BuildContext context, double width) {
    return ScopedModelDescendant<MainModel>(
      builder: (BuildContext context, Widget child, MainModel model) {
        return Container(
          width: width,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              IconButton(
                icon: Icon(Icons.info),
                color: Theme.of(context).accentColor,
                onPressed: () => onProductDetailClick(context, model),
              ),
              _isFavoriting 
                ? IconButton(
                    icon: Container(
                      margin: EdgeInsets.all(6.0),
                      child: CircularProgressIndicator(
                        strokeWidth: 3.0,
                      ),
                    ),
                    color: Theme.of(context).accentColor,
                    onPressed: () {
                      
                    })
                : IconButton(
                    icon: widget.product.isFavorite ? Icon(Icons.favorite) : Icon(Icons.favorite_border),
                    color: Theme.of(context).accentColor,
                    onPressed: () => onAddToFavClick(model)),
            ],
          ),
        );
      },
    );
  }
}